# EEE3088F Pi Hat Projectt
The pi HAT we are making is a temperature sensor Pi HAT. This HAT reads the temperature of any environment it is put in provided its temperature is wihin the HAT's range.

The Hat will be attached to a predesigned temperature sensor PT100. The sensor is to  measure the temperature of its surroundings as a function of voltage and this in turn will be converted to degrees celcious using mathematics and an analogue to digital convertor. Eamples of places where the microHat can be used are a in electronic devices or in home automation systems
such as a geyser, home weather station to measure environmental temperature etc.
It also monitors the temperature of the surroundings. It has 3 LED lights that light up at different temperature ranges i.e. Red LED for 0-2V (0-30°C), Green LED for 2-3.2 V( 30 -59°C) and Blue LED for 3.3V (60°C) notifying the users what temperature range the HAT is in. 
